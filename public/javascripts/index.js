$(function() {
	'use strict';

	if (location.pathname == '/monitor') {
		new Monitor();
	}
});

/**
 * Страница мониторинга
 */
function Monitor() {
	var domSelector = '#graphic-1';
	var remote = new RemoteSource({interval: 300});
	var stream = new BufferedStream({debug: false});

	var options = {
		stream: 1,
		scale: 4,
		block: 0,
	};

	var d3 = new D3Graphic({container: domSelector, scale: options.scale});

	function draw() {
		var started = new $.Deferred();
		var noSoundBlockError = 0;

		remote.startPolling(function data() {
			return options;
		}, function onSuccess(response) {
			if (response.status != 'success') {
				if (response.code == 10 && noSoundBlockError < 30) {
					noSoundBlockError++;
					return true;
				}

				console.warn(response);
				d3.stop();
				return false;
			}

			noSoundBlockError = 0;
			options.block++;
			stream.add(response.block);

			started.resolve();
		}, function onFail() {
			console.warn('Ошибка, остановка RemoteSource');
			remote.stopPolling();
		});

		started.done(function() {
			console.log("Поехали!");
			d3.start(stream);
		});
	}

	// Получаем доступные потоки
	remote.fetch('streams', {})
		.done(function(response) {
			var markup = '<ul class="stream-selector">';
			for (var key in response.streams) {
				markup +=
				'<li class="act-stream-selector" data-stream-id="' + response.streams[key].id + '" title="Размер ' + response.streams[key].size + ' блока(ов)">' +
					response.streams[key].id +
				'</li>';
			}
			markup += "</ul>";

			$(domSelector).find('.graphic-controls').html(markup);

			// Запуск рисования
			$(domSelector).on('click', '.act-stream-selector', function() {
				$(domSelector).find('.active-selector').removeClass('active-selector');
				$(this).addClass('active-selector');

				options.stream = $(this).data('stream-id');
				options.block = 0;

				draw();
			});
		});
}

/**
 * Буфферизация потока
 *
 * @param {Object} params Параметры инициализации
 */
function BufferedStream(params) {
	'use strict';

	var defaults = {};
	var options = $.extend({}, defaults, params);

	function debug() {
		if (options.debug === true) {
			console.debug.apply(console, arguments);
		}
	}

	var data = [];

	/**
	 * Добавить блок данных в поток
	 *
	 * @param {array} list
	 * @return this
	 */
	function add(list) {
		data = data.concat(list);
		debug("buffer size", data.length);
		return this;
	}

	/**
	 * Получить блок данных из потока
	 *
	 * @param {number} shift Сброс элементов
	 * @param {number} size  Возвращаемое количество
	 * @return {array}
	 */
	function get(shift, size) {
		var frame = data.slice(0, size);
		if (data.length >= size) {
			data.splice(0, shift);
		} else {
			if (frame.length < size) {
				var leftPad = [];
				leftPad.length = size - frame.length;
				leftPad.fill(0);
				frame = leftPad.concat(frame);
			}
		}
		return frame;
	}

	function size() {
		return data.length;
	}

	this.add = add;
	this.get = get;
	this.size = size;
}

/**
 * Рисование графика
 *
 * @param {string} container Идентификатор контейнера
 */
function D3Graphic(params) {
	'use strict';

	var defaults = {
		scale: 1,
		container: '',
		samplingFrequncey: 2000,	// Количество пиков в секунду (@todo сделать получение с сервеа)
		refreshFrequency: 20,		// Частота перерисовки графика
		expectedFameSize: null, 	// Размер доступной области для рисования
		delta: null					// Дельта изменения массива данных
	};

	var options = $.extend({}, defaults, params);

	// Размер дельты при отрисовке
	options.delta = (options.samplingFrequncey / options.scale) / options.refreshFrequency;

	var container = $(options.container).find('.graphic-canvas').get(0);

	var canvas;
	var height = $(container).height();
	var width  = $(container).width();

	var scaleX = 0.2;						// Максимальное зачение дельты X
	options.expectedFameSize = width / scaleX; 	// Ожидаемый размер фрейма для заполнения всего окна

	/**
	 * Инициализация канвы
	 */
	(function init() {
		canvas = d3.select(container)
            .append('svg')
            .attr('width', width)
            .attr('height', height);
	})();

	var x = d3.scale.linear()
		.range([0, scaleX]);

	var y = d3.scale.linear()
		.range([height / 2, height / -2]);

	var line = d3.svg.line()
        .interpolate("basis")
        .x(function(d, i) { return x(i); })
        .y(function(d, i) { return y(d); });

	var path = canvas
        .append('path')
            .attr("class", "line");

	/**
	 * Отрисовка данных
	 *
	 * @param {array} data Массив точек
	 */
	function draw(data) {
		path.attr('d', function(d) { return line(data); });
	}

	var timeout, stopped = false;

	/**
	 * Запуск отрисовки
	 *
	 * @param  {BufferedStream} stream
	 */
	function start(stream) {
		if (stopped) return false;

		draw(stream.get(options.delta, options.expectedFameSize));
		var time = Math.round((1 / options.refreshFrequency) * 1e3);

		console.log("stream size=" + stream.size(), " timeout=" + time);

		timeout = setTimeout(function() { start(stream); }, time);
	}

	function stop() {
		stopped = true;
		clearTimeout(timeout);
	}

	this.start = start;
	this.stop  = stop;
}

/**
 * Сервис по работе с удаленным источником данных
 *
 * @param {Object} params
 */
function RemoteSource(params) {
	var defaults = {
		url: location.origin + '/monitor/',
		interval: 200
	};

	var options = $.extend({}, defaults, params);

	function fetch(action, data) {
		return $.ajax({
			url: options.url + action,
			data: data,
			type: 'get'
		});
	}

	function fetchBlock(data) {
		return fetch('block', data);
	}

	var interval;
	/**
	 * Полинг сервера
	 *
	 * @param  {function} data
	 * @param  {function} success
	 * @param  {function} fail
	 */
	function startPolling(data, success, fail) {
		clearInterval(interval);
		var firstCall = new $.Deferred();

		fetchBlock(data())
			.done(function(response) {
				if (success(response) === false) {
					firstCall.reject();
					return false;
				}
				firstCall.resolve();
			})
			.fail(fail);

		// Если первый вызов успешно завершен переходим к интервалу
		firstCall.done(function() {
			interval = setInterval(function() {
				fetchBlock(data())
					.done(function(response) {
						if (success(response) === false) {
							clearInterval(interval);
							return false;
						}
					})
					.fail(fail);
			}, options.interval);
		});
	}

	function stopPolling() {
		clearInterval(interval);
	}

	this.fetch = fetch;
	this.startPolling = startPolling;
	this.stopPolling  = stopPolling;
}
