DROP TABLE IF EXISTS `Christina`.`User`;

CREATE TABLE `Christina`.`User` (
    `id` INT NOT NULL AUTO_INCREMENT ,
    `login` VARCHAR(30) NOT NULL ,
    `password` VARCHAR(50) NOT NULL ,
    `group` INT NOT NULL ,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB;

ALTER TABLE `User` ADD UNIQUE INDEX(`login`);
ALTER TABLE `User` ADD INDEX(`password`, `group`);

INSERT INTO `Christina`.`User` (`id`, `login`, `password`, `group`) VALUES (NULL, 'supervisor', '69cc872243cd201251c9b650e78a92a238be4052', '1');