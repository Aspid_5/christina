package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import domain.audio.Algorithm;
import domain.metric.MetricService;
import domain.metric.NoSoundStreamAvailableException;
import domain.metric.SoundBlock;
import domain.metric.SoundStream;
import domain.remote.SID;
import domain.remote.RemoteServer;
import domain.remote.SessionService;

import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * API для агрегирующего сервера
 */
public class Remote extends Controller {

    private List<Byte> buffer = new ArrayList<>();
    private int index = 0;

    /**
     * Запуск сессии АС
     *
     * /api/session/start
     */
    public Result start() {
        SID sid = SessionService.getInstance().start(new RemoteServer(request().remoteAddress()));
        Logger.debug("Новое соединение " + sid + " " + SessionService.getInstance().getServer(sid).address());

        // @todo Отдельные буфферы и индексы под каждый сенсор

        buffer = new ArrayList<>();
        index  = 0;

        ObjectNode result = Json.newObject();
        result
            .put("status", "success")
            .put("sid", sid.toString());

        return ok(result);
    }

    /**
     * Запись блока данных
     *
     * /api/data/write
     */
    public Result write() {
        int frequency;  // Частота дискретизации
        int sampleSize; // Количество байт в сэмпле
        int dataSize;
        Boolean isBigEndian;

        ObjectNode json = Json.newObject();

        Logger.debug("/api/data/write from " + request().remoteAddress() + ":");
        Map<String, String[]> request = request().body().asFormUrlEncoded();

        String data   = request.get("data")[0];      // Блок данных
        String sid    = request.get("sid")[0];       // Идентификатор сессии
        String sensor = request.get("sensorId")[0];  // Идентификатор звуковго потока

        // @todo Порядковые номера пакетов
        // @todo Авторизация

        try {
            dataSize    = Integer.parseInt(request.get("dataSize")[0]);
            frequency   = Integer.parseInt(request.get("frequency")[0]);
            sampleSize  = Integer.parseInt(request.get("bps")[0]);
            isBigEndian = Boolean.parseBoolean(request.get("isBigEndian")[0]);
        } catch (NumberFormatException exception) {
            json.put("status", "fail").put("message", "Не удалось определить параметр(ы) frequency/bps/isBigEndian");
            return ok(json);
        }

        SoundStream stream;

        try {
            stream = MetricService.getInstance()
                .prepareStream(sensor)
                .getStream(sensor);
        } catch (NoSoundStreamAvailableException exception) {
            json.put("status", "error").put("message", String.format("Поток %s недоступен", sensor));
            return ok(json);
        }

        buffer = new ArrayList<>();
        for (Byte value : data.getBytes(StandardCharsets.ISO_8859_1)) {
            buffer.add(value);
        }

        // Размер буфера, при котором его можно сбросить в звуковой поток
        int expectedSize = sampleSize * stream.getBlockSize();

        try {
            while (buffer.size() > expectedSize) {
                Algorithm decoder = new Algorithm(sampleSize, isBigEndian);

                Byte[] slice = new Byte[expectedSize];
                for (int i = 0; i < expectedSize; i++) {
                    slice[i] = buffer.get(i);
                }

                stream.addBlock(new SoundBlock(
                    this.index,
                    decoder.decodeBytes(slice)
                ));

                buffer = buffer.subList(expectedSize, buffer.size());

                this.index++;
            }
        } catch (Exception exception) {
            Logger.error("Ошибка декодирования аудио: " + exception.getMessage());
            exception.printStackTrace();
            json.put("status", "error").put("message", exception.getMessage());
            return ok(json);
        }

        json.put("status", "success");
        return ok(json);
    }
}
