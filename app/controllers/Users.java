package controllers;

import domain.User;
import play.Logger;
import play.data.Form;
import play.mvc.*;
import storage.Factory;
import views.html.Users.index;
import views.html.Users.add;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

public class Users extends Controller {

    public static class UserForm {

        public String login;
        public String password;
    }

    @Security.Authenticated(Secured.class)
    public Result index() throws SQLException {
        List<User> list = Factory.getInstance().getUserDAO().read();
        return ok(index.render(list));
    }

    @Security.Authenticated(Secured.class)
    public Result addForm() {
        markup.BreadCrumbs.getInstance()
            .put("Пользователи", "/users")
            .put("Добавление пользователя", "");

        return ok(add.render());
    }

    @Security.Authenticated(Secured.class)
    public Result addHandler() {
        Form<UserForm> userForm = Form.form(UserForm.class).bindFromRequest();

        if (userForm.hasErrors()) {
            return badRequest(add.render());
        }

        try {
            Factory.getInstance().getUserDAO()
                .create(new User(userForm.get().login, userForm.get().password));
        } catch (NoSuchAlgorithmException|SQLException exception) {
            Logger.error(exception.getMessage(), exception);
        }

        return redirect(routes.Users.index());
    }
}
