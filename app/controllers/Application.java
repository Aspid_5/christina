package controllers;

import domain.Authorize;
import domain.User;
import play.data.Form;
import play.mvc.*;
import views.html.*;

import java.sql.SQLException;

public class Application extends Controller {

    /**
     * Форма авторизации
     */
    public static class LoginForm {

        public String login;
        public String password;

        public LoginForm() {}

        public LoginForm(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public String validate() {
            User user = Authorize.getInstance().login(this.login, this.password);

            if (user == null) {
                return "Неверный логин или пароль";
            }

            session("login", user.getLogin());

            return null;
        }
    }

    public Result login() {
        Form<LoginForm> form = Form.form(LoginForm.class);

        if (session().get("lastLogin") != null) {
            form.fill(new LoginForm(session().get("lastLogin"), ""));
        }

        return ok(login.render(form));
    }

    @Security.Authenticated(Secured.class)
    public Result logout() {
        Authorize.getInstance().logout();
        return redirect(routes.Application.login());
    }

    public Result authenticate() {
        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            session("lastLogin", loginForm.field("login").valueOr(""));
            return badRequest(login.render(loginForm));
        }

        return redirect(routes.Application.index());
    }

    @Security.Authenticated(Secured.class)
    public Result index() throws SQLException {
        return ok(index.render());
    }
}
