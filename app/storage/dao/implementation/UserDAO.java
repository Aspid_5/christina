package storage.dao.implementation;

import domain.User;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import play.Logger;
import storage.Hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements storage.dao.User {

    private int limit = 50;
    private int offset = 0;

    private List<Criterion> criterion = new ArrayList<>();

    public void create(User user) throws SQLException {
        Session session = null;
        try {
            session = Hibernate.getInstance().getSessionFactory().openSession();
            session.getTransaction().begin();
            session.save(user);
            session.getTransaction().commit();
        } catch (Exception exception) {
            Logger.error("Ошибка I/O", exception);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void update(User user) throws SQLException {
        Session session = null;
        try {
            session = Hibernate.getInstance().getSessionFactory().openSession();
            session.getTransaction().begin();
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception exception) {
            Logger.error("Ошибка I/O", exception);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(User user) throws SQLException {
        Session session = null;
        try {
            session = Hibernate.getInstance().getSessionFactory().openSession();
            session.getTransaction().begin();
            session.delete(user);
            session.getTransaction().commit();
        } catch (Exception exception) {
            Logger.error("Ошибка I/O", exception);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public User read(Integer id) throws SQLException {
        Session session = null;
        User user = null;

        try {
            session = Hibernate.getInstance().getSessionFactory().openSession();
            user = session.get(User.class, id);
        } catch (Exception exception) {
            Logger.error("Ошибка I/O", exception);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return user;
    }

    public UserDAO limit(int limit) {
        this.limit = limit > 0 ? limit : 50;
        return this;
    }

    public UserDAO offset(int offset) {
        this.offset = offset >= 0 ? offset : 0;
        return this;
    }

    public UserDAO where(Criterion criterion) {
        this.criterion.add(criterion);
        return this;
    }

    @SuppressWarnings("unchecked")
    public List<User> read() throws SQLException {
        Session session = null;
        List<User> users = new ArrayList<>();

        try {
            session = Hibernate.getInstance().getSessionFactory().openSession();
            Criteria query = session.createCriteria(User.class)
                .setFirstResult(this.offset)
                .setMaxResults(this.limit);

            this.criterion.forEach(query::add);

            users = query.list();

            // Сброс критериев выборки
            this.criterion = new ArrayList<>();
        } catch (Exception exception) {
            Logger.error("Ошибка I/O", exception);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return users;
    }

    public User readFirst() throws SQLException {
        this.offset(0).limit(1);
        List<User> users = this.read();
        return users.isEmpty() ? null : users.get(0);
    }
}
