package domain.audio;

public class Algorithm {

    private int sampleSize = 0;
    private boolean isBigEndian = true;

    /**
     * @param sampleSize  Размер сэмпла в байтах
     * @param isBigEndian Порядок байтов
     */
    public Algorithm(int sampleSize, boolean isBigEndian) {
        this.sampleSize = sampleSize;
        this.isBigEndian = isBigEndian;
    }

    /**
     * Декодирование аудио байт в сэмплы
     *
     * @param audioBytes WAV-байты
     */
    public double[] decodeBytes(Byte[] audioBytes) {
        double[] audioSamples = new double[audioBytes.length / sampleSize];
        int[] sampleBytes = new int[sampleSize];

        int k = 0; // index in audioBytes
        for (int i = 0; i < audioSamples.length; i++) {
            // collect sample byte in big-endian order
            if (isBigEndian) {
                // bytes start with MSB
                for (int j = 0; j < sampleSize; j++) {
                    sampleBytes[j] = audioBytes[k++];
                }
            } else {
                // bytes start with LSB
                for (int j = sampleSize - 1; j >= 0; j--) {
                    sampleBytes[j] = audioBytes[k++];
                    if (sampleBytes[j] != 0)
                        j = j + 0;
                }
            }

            // get integer value from bytes
            int ival = 0;
            for (int j = 0; j < sampleSize; j++) {
                ival += sampleBytes[j];
                if (j < sampleSize - 1) ival <<= 8;
            }

            // decode value
            double ratio = Math.pow(2., sampleSize * 8 - 1);
            double val = ((double) ival) / ratio;
            audioSamples[i] = val;
        }

        return audioSamples;
    }
}
