package domain;

import org.hibernate.criterion.Restrictions;
import play.Logger;
import storage.Factory;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static play.mvc.Controller.session;

public class Authorize {

    private static Authorize instance = new Authorize();

    public static synchronized Authorize getInstance() {
        return instance;
    }

    /**
     * Выполнить авторизацию пользователя. Вернет объект пользователя в случае успешной авторизации, иначе null.
     *
     * @param login    Логин
     * @param password Пароль
     * @return User|null
     */
    public User login(String login, String password) {
        User user;

        try {
            user = Factory.getInstance()
                .getUserDAO()
                .where(Restrictions.eq("login", login))
                .where(Restrictions.eq("password", User.password(password)))
                .readFirst();

            if (user != null) {
                this.setUser(user);
                Logger.debug(String.format("Пользователь %s авторизован", user.getLogin()));
            }

        } catch (SQLException|NoSuchAlgorithmException exception) {
            Logger.error(exception.getMessage(), exception);
            user = null;
        }

        return user;
    }

    /**
     * Выйти из системы
     */
    public void logout() {
        User user = this.getUser();
        Logger.debug(String.format("Пользователь %s вышел из системы", user.getLogin()));
        session().clear();
    }

    /**
     * Получение текущего авторизованного пользователя
     *
     * @return Текущий пользователь
     */
    public User getUser() {
        User user = new User();

        try {
            try {
                Integer id = new Integer(session().get("id"));
                user = Factory.getInstance().getUserDAO().read(id);
            } catch (NumberFormatException ignored) {}
        } catch (SQLException exception) {
            Logger.error(exception.getMessage(), exception);
        }

        return user;
    }

    private void setUser(User user) {
        session().clear();
        session("id", String.valueOf(user.getId()));
    }
}
