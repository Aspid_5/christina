package domain.remote;

import java.util.HashMap;
import java.util.Map;

public class SessionService {

    private Map<String, RemoteServer> servers = new HashMap<>();

    private static SessionService ourInstance = new SessionService();

    public static SessionService getInstance() {
        return ourInstance;
    }

    /**
     * Создать сессию
     *
     * @return Идетификатор сессии
     */
    public SID start(RemoteServer server) {
        SID sid = SID.generate();
        servers.put(sid.toString(), server);
        return sid;
    }

    /**
     * Проверить валидность сессии
     */
    public boolean check(SID sid, String address) {
        return servers.containsKey(sid.toString()) && servers.get(sid.toString()).address().equals(address);
    }

    /**
     * Получить АС по идентификатору
     */
    public RemoteServer getServer(SID sid) {
        return servers.get(sid.toString());
    }

    /**
     * Сброс всех сессий
     */
    public void clean() {
        servers = new HashMap<>();
    }

    public Map<String, RemoteServer> getServers() {
        return servers;
    }

    private SessionService() {}
}
