package domain.remote;

import org.joda.time.DateTime;

/**
 * Агрегирующий сервер
 */
public class RemoteServer {
    private String address;
    private DateTime created = new DateTime();

    public RemoteServer(String address) {
        this.address = address;
    }

    public String address() {
        return address;
    }

    public DateTime created() {
        return created;
    }
}
