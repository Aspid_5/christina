package domain.remote;

import java.util.UUID;

/**
 * Работа с сессионным идентификатором АС
 */
public class SID {
    UUID sid;

    private SID() {
        this.sid = UUID.randomUUID();
    }

    private SID(String sid) {
        if (sid == null) {
            throw new IllegalArgumentException(sid);
        }
        this.sid = UUID.fromString(sid);
    }

    public static SID generate() {
        return new SID();
    }

    public static SID fromString(String uid) {
        return new SID(uid);
    }

    @Override
    public String toString() {
        return this.sid.toString();
    }
}
