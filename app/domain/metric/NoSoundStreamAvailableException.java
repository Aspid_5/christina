package domain.metric;

public class NoSoundStreamAvailableException extends Exception {

    NoSoundStreamAvailableException (String id) {
        super("Sound stream id <" + id + ">");
    }
}
