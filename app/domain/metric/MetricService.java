package domain.metric;

import java.util.Map;
import java.util.HashMap;

/**
 * Управление метриками пациентов (ФКГ). Фасад пакета metric.
 */
public class MetricService {

    private static MetricService instance = new MetricService();
    private Map<String, SoundStream> sounds = new HashMap<>();

    public static MetricService getInstance() {
        return instance;
    }

    /**
     * Добавить блок звука по пациенту
     *
     * @param id    Идентификатор потока (может = идентификатору пацинета)
     * @param block Блок звука
     */
    public void addBlock(String id, SoundBlock block) {
        this.sounds.get(id).addBlock(block);
    }

    /**
     * Получить поток звука по пацинету
     *
     * @param id Идентификатор потока
     */
    public SoundStream getStream(String id) throws NoSoundStreamAvailableException {
        if (!sounds.containsKey(id)) {
            throw new NoSoundStreamAvailableException(id);
        }
        return sounds.get(id);
    }

    /**
     * Подготовка потока для работы
     */
    public MetricService prepareStream(String id) {
        if (!sounds.containsKey(id)) {
            sounds.put(id, new SoundStream());
        }
        return this;
    }

    /**
     * Полная очистка
     */
    public void clean()
    {
        sounds = new HashMap<>();
    }

    public Map<String, SoundStream> getStreams() {
        return sounds;
    }

    private MetricService() {}
}
