package domain.metric;

import java.util.ArrayList;
import java.util.List;

/**
 * Звуковой поток (ФКГ)
 */
public class SoundStream {

    private List<SoundBlock> stream = new ArrayList<>();
    private int blockSize = SoundBlock.DEFAULT_SIZE;
    private int scale = 1;

    public SoundStream() {}

    public SoundStream(int blockSize) {
        this.blockSize = blockSize;
    }

    /**
     * Добавить новый блок в звуковой поток
     */
    public void addBlock(SoundBlock block) {
        if (block.getSize() != this.blockSize) {
            throw new RuntimeException(String.format("Неожиданный размер блока звука %d, ожидается %d", block.getSize(), this.blockSize));
        }
        stream.add(block.getNumber(), block);
    }

    /**
     * Получить блок данных
     *
     * @param index Индекс блока
     * @return Блок данных
     * @throws NoSoundBlockAvailableException Данные отсутствуют
     */
    public synchronized double[] getBlock(int index) throws NoSoundBlockAvailableException {
        SoundBlock block;

        try {
            block = this.stream.get(index);
        } catch (IndexOutOfBoundsException exception) {
            throw new NoSoundBlockAvailableException();
        }

        return this.scale == 1
                ? block.getSamples()
                : this.scaling(block);
    }

    /**
     * Размер потока в блоках
     */
    public synchronized int size() {
        return stream.size();
    }

    /**
     * Установить коэффицинет масштабирования. Влияет ТОЛЬКО на получение данных из потока.
     */
    public SoundStream scale(int scale) {
        if (scale % 2 != 0) {
            throw new RuntimeException("Коэффициент масштабирования должен быть кратен 2");
        }
        this.scale = scale;
        return this;
    }

    /**
     * Выполнение масштабирования блока звука
     */
    private double[] scaling(SoundBlock block) {
        double[] source = block.getSamples();

        // Выходные данные
        int size = block.getSize() / this.scale;
        double[] buffer = new double[size];

        double sum = 0;
        for (int i = 0, j = 0; i < source.length; i++) {
            sum += source[i];
            if (i % scale == 0) {
                sum /= scale;
                buffer[j++] = sum;
                sum = 0;
            }
        }

        return buffer;
    }

    public int getBlockSize() {
        return blockSize;
    }
}
