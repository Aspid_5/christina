package domain.metric;

/**
 * Блок данных по звуку (ФКГ)
 */
public class SoundBlock {

    public static final int DEFAULT_SIZE = 2000;

    private int number;
    private int size;
    private double[] samples;

    public SoundBlock(int number, double[] samples) {
        this.number  = number;
        this.samples = samples;
        this.size    = samples.length;
    }

    public int getNumber() {
        return number;
    }

    public int getSize() {
        return size;
    }

    public double[] getSamples() {
        return samples;
    }
}
