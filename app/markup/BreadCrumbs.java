package markup;

import java.util.ArrayList;
import java.util.List;

import play.mvc.Http.Context;

/**
 * Хлебные крошки
 */
public class BreadCrumbs {

    public static class Pair {

        private String key;
        private String value;

        public Pair(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

    private List<Pair> breadcrumbsMap = new ArrayList<>();

    private BreadCrumbs() {}

    public static BreadCrumbs getInstance() {
        BreadCrumbs breadCrumbs = (BreadCrumbs) Context.current().args.get("BreadCrumbs");

        if (breadCrumbs == null) {
            breadCrumbs = new BreadCrumbs();
        }

        Context.current().args.put("BreadCrumbs", breadCrumbs);

        return breadCrumbs;
    }

    public BreadCrumbs put(String name, String url) {
        this.breadcrumbsMap.add(new Pair(name, url));
        return this;
    }

    public List get() {
        return this.breadcrumbsMap;
    }

    public String heading() {
        if (breadcrumbsMap.size() > 0) {
            return this.breadcrumbsMap.get(0).getKey();
        } else {
            return null;
        }
    }
}
